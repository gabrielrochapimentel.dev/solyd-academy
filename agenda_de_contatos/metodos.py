agenda = {
    "Gabriel": {
        "telefone": "135151351",
        "email": "gabriel@gmail.com",
        "rua": "rua aleatoria do brasil"
    },
    "Luana": {
        "telefone": "231651351",
        "email": "luana@gmail.com",
        "rua": "rua aleatoria do brasil"
    }
}

#Criando um contato
def criar_contato():
    while True:
        nome = str(input("Insira o nome do contato: "))
        telefone = str(input("Insira o telefone do contato: "))
        email = str(input("Insira o email do contato: "))
        rua = str(input("Insira o endereço do contato: "))

        agenda.update({
            nome: { "telefone": telefone, "email": email, "rua": rua }
        })

        stop = input("\nDigite SIM para PARAR: \t")
        stop.upper
        if stop[0] == 's':
            break
            

# Deletando um contato
def deletar_contato(valor):
    agenda.pop(valor)


def pesquisar_contato(nome=""):
    print(f"\n-+--+--+--+--+--+- AGENDA -+--+--+--+--+--+-")
    
    if len(agenda) == 0:
        print("A agenda está vazia...")
    else:
        if len(nome) > 0:
            data = agenda[nome]
            print(nome)
            for chave, info in data.items():
                print(f"{chave}: {info}")
        else:
            for chave, valor in agenda.items():
                print(f"\nNOME: {chave}\n")

                for info, result in valor.items():
                    print(f"{info}: {result}")
    print(f"\n-+--+--+--+--+--+--+- FIM -+--+--+--+--+--+--+-")